using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FixedSizedQueueWithTimer.Tests {
	public class FixedSizedQueueWithTimerTests {
		[TestCase(1, 1)]
		[TestCase(5, TimeSpan.TicksPerMinute)]
		[TestCase(100, long.MaxValue)]
		public void ConstructorValidArgumentsTest(int size, long ticksToLive) {
			// ReSharper disable ObjectCreationAsStatement
			Assert.DoesNotThrow(() => new FixedSizedQueueWithTimer<int>(size, new TimeSpan(ticksToLive)));
			// ReSharper restore ObjectCreationAsStatement
		}

		[TestCase(0, 1)]
		[TestCase(-1, TimeSpan.TicksPerMinute)]
		[TestCase(1, 0)]
		[TestCase(5, -1)]
		[TestCase(int.MinValue, long.MaxValue)]
		[TestCase(int.MinValue, long.MinValue)]
		public void ConstructorInvalidArgumentsTest(int size, long ticksToLive) {
			// ReSharper disable ObjectCreationAsStatement
			Assert.Catch<ArgumentException>(() => new FixedSizedQueueWithTimer<int>(size, new TimeSpan(ticksToLive)));
			// ReSharper restore ObjectCreationAsStatement
		}

		[TestCase(1)]
		[TestCase(5)]
		[TestCase(ushort.MaxValue)]
		public async Task SingleObjectNotExpiredEnqueueAndDequeueTest(int size) {
			FixedSizedQueueWithTimer<int> queue = new FixedSizedQueueWithTimer<int>(size, TimeSpan.FromSeconds(1));

			Assert.AreEqual(0, queue.Count);
			Assert.AreEqual(size, queue.Size);

			queue.Enqueue(1);
			await Task.Delay(500).ConfigureAwait(false);

			Assert.AreEqual(1, queue.Count);
			Assert.IsTrue(queue.TryDequeue(out int value));
			Assert.AreEqual(1, value);
			Assert.AreEqual(0, queue.Count);
		}

		[TestCase(1)]
		[TestCase(5)]
		[TestCase(ushort.MaxValue)]
		public async Task SingleObjectExpiredEnqueueAndDequeueTest(int size) {
			FixedSizedQueueWithTimer<int> queue = new FixedSizedQueueWithTimer<int>(size, TimeSpan.FromSeconds(1));

			Assert.AreEqual(0, queue.Count);
			Assert.AreEqual(size, queue.Size);

			queue.Enqueue(1);
			await Task.Delay(1001).ConfigureAwait(false);

			Assert.AreEqual(0, queue.Count);
			Assert.IsFalse(queue.TryDequeue(out _));
		}

		[TestCase(1)]
		[TestCase(5)]
		public async Task FullQueueEnqueueWithAutoDequeueTest(int size) {
			FixedSizedQueueWithTimer<int> queue = new FixedSizedQueueWithTimer<int>(size, TimeSpan.FromSeconds(5));
			Assert.AreEqual(0, queue.Count);
			Assert.AreEqual(size, queue.Size);

			TimeSpan delay = TimeSpan.FromTicks(5 * TimeSpan.TicksPerSecond / size);
			for (int i = 0; i < size; i++) {
				queue.Enqueue(i);

				if (i < size - 1) {
					await Task.Delay(delay).ConfigureAwait(false);
				}
			}

			await Task.Delay(TimeSpan.FromTicks(10)).ConfigureAwait(false);
			Assert.AreEqual(size, queue.Count);

			for (int i = 0; i < size; i++) {
				Assert.IsTrue(queue.TryPeek(out int value));
				Debug.WriteLine(value + " " + queue.Count);
				Assert.AreEqual(i, value);
				Assert.AreEqual(size - i, queue.Count);
				await Task.Delay(delay).ConfigureAwait(false);
			}
		}

		[TestCase(1)]
		[TestCase(5)]
		public async Task FullQueueWithOverflowEnqueueWithAutoDequeueTest(int size) {
			FixedSizedQueueWithTimer<int> queue = new FixedSizedQueueWithTimer<int>(size, TimeSpan.FromSeconds(10));
			Assert.AreEqual(0, queue.Count);
			Assert.AreEqual(size, queue.Size);

			TimeSpan delay = TimeSpan.FromTicks(5 * TimeSpan.TicksPerSecond / size);
			for (int i = 0; i < size * 2; i++) {
				queue.Enqueue(i);

				if (i < size * 2 - 1) {
					await Task.Delay(delay).ConfigureAwait(false);
				}
			}

			await Task.Delay(TimeSpan.FromSeconds(6)).ConfigureAwait(false);
			Assert.AreEqual(size, queue.Count);

			for (int i = size; i < size * 2; i++) {
				Assert.IsTrue(queue.TryPeek(out int value));
				Debug.WriteLine(DateTime.UtcNow.ToString("O") + "|" + i + "|" + value);
				Assert.AreEqual(i, value);
				Assert.AreEqual(size * 2 - i, queue.Count);
				await Task.Delay(delay).ConfigureAwait(false);
			}
		}
	}
}
