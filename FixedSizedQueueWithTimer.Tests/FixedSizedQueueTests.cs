using System;
using NUnit.Framework;

namespace FixedSizedQueueWithTimer.Tests {
	public class FixedSizedQueueTests {
		[TestCase(1)]
		[TestCase(5)]
		[TestCase(100)]
		public void ConstructorValidArgumentsTest(int size) {
			// ReSharper disable ObjectCreationAsStatement
			Assert.DoesNotThrow(() => new FixedSizedQueue<int>(size));
			// ReSharper restore ObjectCreationAsStatement
		}

		[TestCase(0)]
		[TestCase(-1)]
		[TestCase(int.MinValue)]
		public void ConstructorInvalidArgumentsTest(int size) {
			// ReSharper disable ObjectCreationAsStatement
			Assert.Catch<ArgumentException>(() => new FixedSizedQueue<int>(size));
			// ReSharper restore ObjectCreationAsStatement
		}

		[TestCase(1)]
		[TestCase(5)]
		[TestCase(ushort.MaxValue)]
		public void SingleObjectEnqueueAndDequeueTest(int size) {
			FixedSizedQueue<int> queue = new FixedSizedQueue<int>(size);

			Assert.AreEqual(0, queue.Count);
			Assert.AreEqual(size, queue.Size);

			queue.Enqueue(1);

			Assert.AreEqual(1, queue.Count);
			Assert.IsTrue(queue.TryDequeue(out int value));
			Assert.AreEqual(1, value);
			Assert.AreEqual(0, queue.Count);
		}

		[TestCase(1)]
		[TestCase(5)]
		[TestCase(ushort.MaxValue)]
		public void FullQueueEnqueueAndDequeueTest(int size) {
			FixedSizedQueue<int> queue = new FixedSizedQueue<int>(size);
			Assert.AreEqual(0, queue.Count);
			Assert.AreEqual(size, queue.Size);

			for (int i = 0; i < size; i++) {
				queue.Enqueue(i);
			}

			Assert.AreEqual(size, queue.Count);

			for (int i = 0; i < size; i++) {
				Assert.IsTrue(queue.TryDequeue(out int value));
				Assert.AreEqual(i, value);
			}
		}

		[TestCase(1)]
		[TestCase(5)]
		[TestCase(ushort.MaxValue)]
		public void FullQueueWithOverflowEnqueueAndDequeueTest(int size) {
			FixedSizedQueue<int> queue = new FixedSizedQueue<int>(size);
			Assert.AreEqual(0, queue.Count);
			Assert.AreEqual(size, queue.Size);

			for (int i = 0; i < size; i++) {
				queue.Enqueue(i);
			}

			Assert.IsTrue(queue.TryPeek(out int value));
			Assert.AreEqual(0, value);

			queue.Enqueue(size);
			Assert.AreEqual(queue.Count, size);
			Assert.IsTrue(queue.TryPeek(out value));
			Assert.AreEqual(1, value);

			for (int i = size; i > 0; i--) {
				queue.Enqueue(i);
			}

			Assert.IsTrue(queue.TryPeek(out value));
			Assert.AreEqual(size, value);
		}
	}
}
