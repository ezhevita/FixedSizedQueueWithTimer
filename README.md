# FixedSizedQueueWithTimer

Library which provides two different queue types:
1. FixedSizedQueue<T> - provides a fixed-sized queue that throws away the first item on adding another item to the full queue
2. FixedSizedQueueWithTimer<T> - enhanced version of FixedSizedQueue that capable of auto-removing items by defined timeout