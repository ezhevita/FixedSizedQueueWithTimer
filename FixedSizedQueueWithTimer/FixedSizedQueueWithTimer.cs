using System;
using System.Threading;
using JetBrains.Annotations;

namespace FixedSizedQueueWithTimer {
	[PublicAPI]
	public class FixedSizedQueueWithTimer<T> : FixedSizedQueue<T>, IDisposable {
		private readonly TimeSpan InfiniteTimeSpan = TimeSpan.FromTicks(-1 * TimeSpan.TicksPerMillisecond);

		public FixedSizedQueueWithTimer(int size, TimeSpan timeToLive) : base(size) {
			if (timeToLive <= TimeSpan.Zero) {
				throw new ArgumentException("Object time-to-live can't be equal to or lower than TimeSpan.Zero", nameof(timeToLive));
			}

			ItemsExpirationDates = size > 1 ? new FixedSizedQueue<DateTime>(size - 1) : null;
			RemovalTimer = new Timer(
				e => TryDequeue(out _, false),
				null,
				-1,
				-1
			);

			TimeToLive = timeToLive;
		}

		public TimeSpan TimeToLive { get; }

		private FixedSizedQueue<DateTime> ItemsExpirationDates { get; }
		private Timer RemovalTimer { get; }

		public override void Enqueue(T obj) {
			lock (SyncObject) {
				DateTime newItemDequeueDate = DateTime.UtcNow.Add(TimeToLive);
				DateTime nextItemDequeueDate = default;
				bool hasDateChanged = (Count == 0) || (Count == Size);

				if (Size > 1) {
					if (Count == Size) {
						nextItemDequeueDate = ItemsExpirationDates.Peek();
					}

					ItemsExpirationDates.Enqueue(newItemDequeueDate);
				}

				base.Enqueue(obj);
				if (hasDateChanged) {
					if (Size > 1) {
						bool result = (Count == Size) || ItemsExpirationDates.TryDequeue(out nextItemDequeueDate);
						UpdateTimer(result ? nextItemDequeueDate.Subtract(DateTime.UtcNow) : InfiniteTimeSpan);
					} else {
						UpdateTimer(newItemDequeueDate.Subtract(DateTime.UtcNow));
					}
				}
			}
		}

		public override bool TryDequeue(out T obj) => TryDequeue(out obj, true);

		internal bool TryDequeue(out T obj, bool shouldUpdateTimer) {
			lock (SyncObject) {
				bool result = base.TryDequeue(out obj);
				if (result) {
					if (Size > 1) {
						bool dequeueResult = ItemsExpirationDates.TryDequeue(out DateTime nextItemDequeueDate);
						UpdateTimer(dequeueResult ? nextItemDequeueDate.Subtract(DateTime.UtcNow) : InfiniteTimeSpan);
					} else {
						UpdateTimer(InfiniteTimeSpan);
					}
				}

				return result;
			}
		}

		private void UpdateTimer(TimeSpan dueTime) {
			RemovalTimer.Change(dueTime, InfiniteTimeSpan);
		}
		
		public void Dispose() {
			RemovalTimer.Dispose();
			Clear();
		}
	}
}
