﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace FixedSizedQueueWithTimer {
	[PublicAPI]
	public class FixedSizedQueue<T> : Queue<T> {
		protected readonly object SyncObject = new object();

		public FixedSizedQueue(int size) : base(size) {
			if (size < 1) {
				throw new ArgumentException(nameof(size) + " can't be lower than 1", nameof(size));
			}

			Size = size;
		}

		public int Size { get; }

		public new virtual void Enqueue(T obj) {
			lock (SyncObject) {
				base.Enqueue(obj);

				while (Count > Size) {
					TryDequeue(out _);
				}
			}
		}

		public virtual bool TryDequeue(out T obj) {
			lock (SyncObject) {
				if (Count == 0) {
					obj = default;
					return false;
				}

				obj = Dequeue();
				return true;
			}
		}

	#if !NETSTANDARD2_1
		public bool TryPeek(out T obj) {
			lock (SyncObject) {
				if (Count == 0) {
					obj = default;
					return false;
				}

				obj = Peek();
				return true;
			}
		}
	#endif
	}
}
